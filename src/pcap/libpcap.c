
#include <nebase/syslog.h>
#include <nebase/net/device.h>
#include <nebase/events.h>

#include <src/pcap/plugin.h>

#include <stdlib.h>
#include <net/ethernet.h>

#include <pcap/pcap.h>

struct pf_libpcap_context {
	const struct pf_pcap_libpcap_conf *conf;
	pcap_t *ph;
};

static void *pf_pcap_prepare(const void *conf, int *retry);
static pf_pcap_ret_t pf_pcap_loop(void *context, pf_pcap_handle_event_fn eh);
static void pf_pcap_finish(void *context);

static struct pf_pcap_plugin_s pf_pcap_plugin = {
	.version = PF_PCAP_PLUGIN_VERSION,
	.conf_version = PF_PCAP_LIBPCAP_CONF_VERSION,
	.prepare = pf_pcap_prepare,
	.loop = pf_pcap_loop,
	.finish = pf_pcap_finish,
};
struct pf_pcap_plugin_s *pf_pcap_plugin_p = &pf_pcap_plugin;

static pcap_t *pf_libpcap_open(const struct pf_pcap_libpcap_conf *conf)
{
	char errbuf[PCAP_ERRBUF_SIZE];

	pcap_t *ph;
	ph = pcap_create(conf->ifname, errbuf);
	if (!ph) {
		neb_syslog(LOG_ERR, "pcap_create(%s): %s", conf->ifname, errbuf);
		return NULL;
	}
#define PCAP_SNAPLEN 65535
#define PCAP_PROMISC 1
#define PCAP_TIMEOUT 100       //100 ms
	pcap_set_snaplen(ph, PCAP_SNAPLEN);
	pcap_set_promisc(ph, PCAP_PROMISC);
	pcap_set_immediate_mode(ph, 1);
	pcap_set_timeout(ph, PCAP_TIMEOUT);
	pcap_set_buffer_size(ph, conf->buffer_size);

	switch (pcap_activate(ph)) {
	case 0:
		break;
	case PCAP_WARNING_PROMISC_NOTSUP:
	case PCAP_WARNING_TSTAMP_TYPE_NOTSUP:
	case PCAP_ERROR_ACTIVATED:
	case PCAP_ERROR_NO_SUCH_DEVICE:
	case PCAP_ERROR_PERM_DENIED:
	case PCAP_ERROR_PROMISC_PERM_DENIED:
	case PCAP_ERROR_RFMON_NOTSUP:
	case PCAP_ERROR_IFACE_NOT_UP:
	case PCAP_WARNING:
	case PCAP_ERROR:
	default:
		neb_syslog(LOG_ERR, "pcap_activate: %s", pcap_geterr(ph));
		pcap_close(ph);
		return NULL;
		break;
	}

	if (pcap_set_datalink(ph, DLT_EN10MB) == -1) {
		neb_syslog(LOG_ERR, "pcap_set_datalink: %s", pcap_geterr(ph));
		pcap_close(ph);
		return NULL;
	}

	struct bpf_program bp;
	if (pcap_compile(ph, &bp, conf->filter, 1, PCAP_NETMASK_UNKNOWN) == -1) {
		neb_syslog(LOG_ERR, "pcap_compile: %s", pcap_geterr(ph));
		pcap_close(ph);
		return NULL;
	}
	if (pcap_setfilter(ph, &bp) == -1) {
		neb_syslog(LOG_ERR, "pcap_setfilter: %s", pcap_geterr(ph));
		pcap_close(ph);
		return NULL;
	}
	pcap_freecode(&bp);

	return ph;
}

static void pf_libpcap_cb(u_char *user, const struct pcap_pkthdr *h, const u_char *buf _nattr_unused)
{
	struct pf_libpcap_context *ctx = (struct pf_libpcap_context *)user;
	// pcap_t *ph = ctx->ph;

	if (h->caplen != h->len) {
		neb_syslog(LOG_ERR, "snaplen need to be increased. Packet len: %u, caplen: %u", h->len, h->caplen);
		return;
	}
	if (h->caplen <= sizeof(struct ether_header))
		return;

	// TODO
}

static void *pf_pcap_prepare(const void *conf, int *retry)
{
	const struct pf_pcap_libpcap_conf *c = conf;
	if (!neb_net_device_is_up(c->ifname)) {
		*retry = 1;
		return NULL;
	}
	*retry = 0;
	struct pf_libpcap_context *ctx = calloc(1, sizeof(struct pf_libpcap_context));
	if (!ctx) {
		neb_syslogl(LOG_ERR, "calloc: %m");
		return NULL;
	}
	ctx->conf = conf;
	ctx->ph = pf_libpcap_open(conf);
	if (!ctx->ph) {
		free(ctx);
		return NULL;
	}
	return ctx;
}

static pf_pcap_ret_t pf_pcap_loop(void *context, pf_pcap_handle_event_fn eh)
{
	struct pf_libpcap_context *ctx = context;
	pcap_t *ph = ctx->ph;

	for (;;) {
		if (thread_events) {
			int quit = eh();
			if (quit)
				return PCAP_RET_TERMINATED;
		}

		int ret = pcap_dispatch(ph, -1, pf_libpcap_cb, context);
		switch (ret) {
		case PCAP_ERROR:
			neb_syslog(LOG_ERR, "pcap_dispatch: %s", pcap_geterr(ph));
			return PCAP_RET_ERROR;
			break;
		case PCAP_ERROR_BREAK:
			return PCAP_RET_FINISHED;
			break;
		default:
			if (ret < 0) {
				neb_syslog(LOG_ERR, "pcap_dispatch(%d): %s", ret, pcap_geterr(ph));
				return PCAP_RET_ERROR;
			}
			continue;
			break;
		}
	}
}

static void pf_pcap_finish(void *context)
{
	struct pf_libpcap_context *ctx = context;
	if (ctx->ph) {
		pcap_close(ctx->ph);
		ctx->ph = NULL;
	}
	free(ctx);
}
