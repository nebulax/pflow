
include_directories(${PCAP_INCLUDE_DIRS})

add_library(pcap_libpcap MODULE libpcap.c common.c)
install(TARGETS pcap_libpcap LIBRARY DESTINATION ${PFLOW_PLUGIN_INSTALL_DIR})

if(OS_LINUX)
  add_library(pcap_pfpacket MODULE pfpacket.c common.c)
  install(TARGETS pcap_pfpacket LIBRARY DESTINATION ${PFLOW_PLUGIN_INSTALL_DIR})
endif(OS_LINUX)

if(OS_FREEBSD)
  add_library(pcap_netmap MODULE netmap.c common.c)
  install(TARGETS pcap_netmap LIBRARY DESTINATION ${PFLOW_PLUGIN_INSTALL_DIR})
endif(OS_FREEBSD)
