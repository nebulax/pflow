
#ifndef PF_PCAP_PLUGIN_H
#define PF_PCAP_PLUGIN_H 1

#include <nebase/cdefs.h>

#include <stdint.h>
#include <net/if.h>

typedef enum {
	PCAP_RET_TERMINATED = 0,
	PCAP_RET_FINISHED,
	PCAP_RET_OFFLINE,
	PCAP_RET_ERROR,
} pf_pcap_ret_t;

/**
 * \return 1 if to break, 0 if continue
 */
typedef int (* pf_pcap_handle_event_fn)(void);

typedef void *(* pf_pcap_prepare_fn)(const void *conf, int *retry);
typedef pf_pcap_ret_t (* pf_pcap_loop_fn)(void *context, pf_pcap_handle_event_fn eh);
typedef void (* pf_pcap_finish_fn)(void *context);

#define PF_PCAP_PLUGIN_VERSION 0
struct pf_pcap_plugin_s {
	uint32_t version;
	uint32_t conf_version;
	pf_pcap_prepare_fn prepare;
	pf_pcap_loop_fn loop;
	pf_pcap_finish_fn finish;
} _nattr_packed;

#define PF_PCAP_PLUGIN_SYMBOL "pf_pcap_plugin_p"
extern struct pf_pcap_plugin_s *pf_pcap_plugin_p;

#define PF_PCAP_LIBPCAP_CONF_VERSION 0
struct pf_pcap_libpcap_conf {
	uint32_t version;
	uint32_t ifindex;
	const char *ifname;
	const char *filter;
	int32_t buffer_size;
} _nattr_packed;

#endif
