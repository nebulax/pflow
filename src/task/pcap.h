
#ifndef PF_TASK_PCAP_H
#define PF_TASK_PCAP_H 1

#include <nebase/cdefs.h>

#include <src/conf/task.h>

extern int pf_task_pcap_thread_create(const struct pf_task_conf *conf)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern int pf_task_pcap_thread_is_running(void)
	_nattr_warn_unused_result;
extern int pf_task_pcap_thread_destroy(void)
	_nattr_warn_unused_result;

#endif
