
#include <nebase/syslog.h>
#include <nebase/thread.h>
#include <nebase/plugin.h>

#include <src/pcap/plugin.h>
#include <src/conf/common.h>

#include "pcap.h"
#include "main.h"

#include <pthread.h>
#include <signal.h>
#include <stdio.h>

// used by main thread
static pthread_t pf_task_pcap_thread_id;
static int pf_task_pcap_thread_running = 0;

// used by pcap thread
static struct pf_pcap_plugin_s *driver = NULL;

static void *load_plugin(const struct pf_task_conf *conf)
{
	const char *plugin_name = pf_conf_task_driver_name(conf->driver);
	char *plugin_path = NULL;
	if (asprintf(&plugin_path, "%s/%s", pf_default_plugin_dir, plugin_name) == -1) {
		neb_syslogl(LOG_ERR, "asprintf: %m");
		return NULL;
	}

	void *plugin = neb_plugin_open(plugin_path);
	if (!plugin) {
		neb_syslog(LOG_ERR, "failed to load pcap plugin %s", plugin_path);
		free(plugin_path);
		return NULL;
	}
	free(plugin_path);

	void *symbol_addr = NULL;
	if (neb_plugin_get_symbol(plugin, PF_PCAP_PLUGIN_SYMBOL, &symbol_addr) != 0) {
		neb_syslog(LOG_ERR, "no symbol %s found in plugin %s", PF_PCAP_PLUGIN_SYMBOL, plugin_name);
		neb_plugin_close(plugin);
		return NULL;
	}
	driver = *(struct pf_pcap_plugin_s **)symbol_addr;

	if (driver->version != PF_PCAP_PLUGIN_VERSION) {
		neb_syslog(LOG_ERR, "invalid plugin version %u for %s, exp %u", driver->version, plugin_name, PF_PCAP_PLUGIN_VERSION);
		neb_plugin_close(plugin);
		return NULL;
	}

	return plugin;
}

static void unload_plugin(void *handle)
{
	neb_plugin_close(handle);
}

static void *pcap_thread_exec(void *arg _nattr_unused)
{
	if (neb_thread_register() != 0)
		pthread_exit(NULL); // FIXME

	const struct pf_task_conf *conf = arg;
	void *plugin = load_plugin(conf);
	if (!plugin)
		pthread_exit(NULL); // FIXME

	// TODO init

	if (neb_thread_set_ready() != 0) {
		// TODO
	}

	// TODO main loop

	unload_plugin(plugin);
	pthread_exit(NULL); // FIXME
}

int pf_task_pcap_thread_create(const struct pf_task_conf *conf)
{
	if (neb_thread_create(&pf_task_pcap_thread_id, NULL, pcap_thread_exec, (void *)conf) != 0)
		return -1;
	pf_task_pcap_thread_running = 1;
	return 0;
}

int pf_task_pcap_thread_is_running(void)
{
	return neb_thread_is_running(pf_task_pcap_thread_id);
}

int pf_task_pcap_thread_destroy(void)
{
	if (!pf_task_pcap_thread_running)
		return 0;
	if (neb_thread_destroy(pf_task_pcap_thread_id, SIGTERM, NULL) != 0) // FIXME
		return -1;
	pf_task_pcap_thread_running = 0;
	return 0;
}
