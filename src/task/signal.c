
#include <nebase/syslog.h>
#include <nebase/signal.h>

#include "signal.h"

#include <stddef.h>

int pf_task_setup_signal_handlers(void)
{
	struct sigaction act;

	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaddset(&act.sa_mask, SIGINT);
	act.sa_handler = neb_sigterm_handler;
	if (sigaction(SIGTERM, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	return 0;
}

int pf_task_unblock_signals(void)
{
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGTERM);

	if (sigprocmask(SIG_UNBLOCK, &set, NULL) == -1) {
		neb_syslog(LOG_CRIT, "sigprocmask: %m");
		return -1;
	}
	return 0;
}
