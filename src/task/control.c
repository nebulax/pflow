
#include <nebase/syslog.h>
#include <nebase/events.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>
#include <nebase/sock/common.h>

#include "control.h"
#include "main.h"

static int capture_running = 0;

static neb_evdp_cb_ret_t pf_task_ctl_rsp(int fd, void *udata, const void *context _nattr_unused)
{
	struct pf_task_proc *ptp = udata;
	if (neb_sock_send_exact(fd, &ptp->ctl_rsp, sizeof(ptp->ctl_rsp)) != 0) {
		neb_syslog(LOG_CRIT, "Failed to send ctl response");
		return NEB_EVDP_CB_BREAK_ERR;
	}

	if (neb_evdp_source_os_fd_next_read(ptp->evdp_s, pf_task_ctl_handle) != 0) {
		neb_syslog(LOG_CRIT, "Failed to set read handler");
		return NEB_EVDP_CB_BREAK_ERR;
	}

	return NEB_EVDP_CB_CONTINUE;
}

static neb_evdp_cb_ret_t handle_ctl_hup(void)
{
	thread_events |= T_E_QUIT;
	return NEB_EVDP_CB_CLOSE;
}

neb_evdp_cb_ret_t pf_task_ctl_hup(int fd _nattr_unused, void *udata _nattr_unused, const void *context _nattr_unused)
{
	neb_syslog(LOG_DEBUG, "Control socket closed");
	return handle_ctl_hup();
}

neb_evdp_cb_ret_t pf_task_ctl_handle(int fd, void *udata, const void *context)
{
	struct pf_task_proc *ptp = udata;

	int nread = 0;
	if (neb_evdp_io_get_nread(context, &nread) == -1) {
		neb_syslog(LOG_ERR, "Failed to get unread ctl msg size");
		return NEB_EVDP_CB_BREAK_ERR;
	}
	if (nread != sizeof(ptp->ctl_msg)) {
		if (nread) {
			neb_syslog(LOG_ERR, "invalid recvmsg size %d, expected %zu", nread, sizeof(ptp->ctl_msg));
		} else {
			neb_syslog(LOG_DEBUG, "Control socket closed");
		}
		return handle_ctl_hup();
	}

	if (neb_sock_recv_exact(fd, &ptp->ctl_msg, sizeof(ptp->ctl_msg)) != 0) {
		neb_syslog(LOG_CRIT, "Failed to recv ctl msg");
		return NEB_EVDP_CB_BREAK_ERR;
	}

	ptp->ctl_rsp.type = PF_CTL_RSP_ERR;
	ptp->ctl_rsp.id = ptp->ctl_msg.id;

	switch (ptp->ctl_msg.type) {
	case PF_CTL_CMD_NOOP:
		ptp->ctl_rsp.type = PF_CTL_RSP_OK;
		break;
	case PF_CTL_CMD_PING:
		ptp->ctl_rsp.type = PF_CTL_RSP_PONG;
		break;
	case PF_CTL_CMD_QUIT:
		if (capture_running) {
			; // TODO stop first
		}
		return NEB_EVDP_CB_BREAK_EXP;
		break;
	case PF_CTL_CMD_START:
		// TODO
		capture_running = 1;
		ptp->ctl_rsp.type = PF_CTL_RSP_OK;
		break;
	case PF_CTL_CMD_STOP:
		if (capture_running) {
			; // TODO stop
		} else {
			ptp->ctl_rsp.type = PF_CTL_RSP_OK;
		}
		break;
	case PF_CTL_CMD_LOAD:
		// TODO
		break;
	default:
		neb_syslog(LOG_CRIT, "Invalid control msg type %d", ptp->ctl_msg.type);
		break;
	}

	if (neb_evdp_source_os_fd_next_write(ptp->evdp_s, pf_task_ctl_rsp) != 0) {
		neb_syslog(LOG_CRIT, "Failed to set write handler");
		return NEB_EVDP_CB_BREAK_ERR;
	}
	return NEB_EVDP_CB_CONTINUE;
}

void pf_task_ctl_clean(void)
{
	if (!capture_running)
		return;
	// TODO
}
