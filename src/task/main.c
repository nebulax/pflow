
#include <nebase/syslog.h>
#include <nebase/sem.h>
#include <nebase/thread.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>

#include <src/task.h>

#include "main.h"
#include "signal.h"
#include "control.h"

#include <unistd.h>

struct pf_task_proc *pf_task_runtime_proc = NULL;

struct pf_task_proc *pf_task_proc_new(struct pf_task_conf *ptc)
{
	struct pf_task_proc *ptp = calloc(1, sizeof(struct pf_task_proc));
	if (!ptp) {
		neb_syslog(LOG_ERR, "calloc: %m");
		return NULL;
	}

	ptp->conf = ptc;
	ptp->ctl_fd = -1;

	// TODO

	return ptp;
}

void pf_task_proc_evdp_detach(struct pf_task_proc *ptp)
{
	if (ptp->evdp_s) {
		neb_evdp_queue_t q = neb_evdp_source_get_queue(ptp->evdp_s);
		if (q) {
			if (neb_evdp_queue_detach(q, ptp->evdp_s, 1) != 0)
				neb_syslog(LOG_ERR, "Failed to detach proc_s %p from queue %p", ptp->evdp_s, q);
		}
		neb_evdp_source_del(ptp->evdp_s);
		ptp->evdp_s = NULL;
	}

	if (ptp->ctl_fd >= 0) {
		close(ptp->ctl_fd);
		ptp->ctl_fd = -1;
	}
}

void pf_task_proc_del(struct pf_task_proc *ptp)
{
	// TODO

	pf_task_proc_evdp_detach(ptp);

	if (ptp->conf) {
		pf_conf_task_del(ptp->conf);
		ptp->conf = NULL;
	}

	free(ptp);
}

static int remove_proc_source(neb_evdp_source_t s)
{
	struct pf_task_proc *ptp = neb_evdp_source_get_udata(s);
	if (!ptp)
		return 0;
	pf_task_proc_evdp_detach(ptp);
	return 0;
}

static int task_ctl_evdp_attach(neb_evdp_queue_t dq)
{
	struct pf_task_proc *ptp = pf_task_runtime_proc;
	ptp->evdp_s = neb_evdp_source_new_os_fd(ptp->ctl_fd, pf_task_ctl_hup);
	if (!ptp->evdp_s) {
		neb_syslog(LOG_ERR, "Failed to create ctl evdp_source");
		return -1;
	}
	neb_evdp_source_set_udata(ptp->evdp_s, ptp);
	neb_evdp_source_set_on_remove(ptp->evdp_s, remove_proc_source);

	if (neb_evdp_queue_attach(dq, ptp->evdp_s) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach ctl evdp_source to evdp_queue");
		pf_task_proc_evdp_detach(ptp);
		return -1;
	}

	if (neb_evdp_source_os_fd_next_read(ptp->evdp_s, pf_task_ctl_handle) != 0) {
		neb_syslog(LOG_ERR, "Failed to set read action");
		pf_task_proc_evdp_detach(ptp);
		return -1;
	}

	return 0;
}

int pf_task_proc_run(void)
{
	int ret = 0;

	if (pf_task_setup_signal_handlers() != 0) {
		neb_syslog(LOG_ERR, "Failed to setup signal handlers");
		return -1;
	}

	if (neb_thread_init() != 0) {
		neb_syslog(LOG_ERR, "Failed to init thread");
		return -1;
	}

	neb_evdp_queue_t dq = neb_evdp_queue_create(0);
	if (!dq) {
		neb_syslog(LOG_ERR, "Failed to create evdp_queue");
		ret = -1;
		goto exit_deinit_t;
	}
	if (task_ctl_evdp_attach(dq) != 0) {
		ret = -1;
		goto exit_destroy_q;
	}

	if (neb_sem_proc_post(pf_task_proc_sem, 0) != 0) {
		neb_syslog(LOG_ERR, "Failed to tell parent we are ready");
		ret = -1;
		goto exit_destroy_q;
	}

	if (pf_task_unblock_signals() != 0) {
		neb_syslog(LOG_ERR, "Failed to unblock signals");
		ret = -1;
		goto exit_destroy_q;
	}

	if (neb_evdp_queue_run(dq) != 0) {
		neb_syslog(LOG_ERR, "Error occure while running evdp_queue");
		ret = -1;
	}
	pf_task_ctl_clean();

exit_destroy_q:
	neb_evdp_queue_destroy(dq);
exit_deinit_t:
	neb_thread_deinit();
	return ret;
}
