
#ifndef PFLOW_TASK_MAIN_H
#define PFLOW_TASK_MAIN_H 1

#include <nebase/cdefs.h>
#include <nebase/evdp/types.h>

#include <src/conf/task.h>

#include "control.h"

#include <sys/queue.h>

struct pf_task_proc {
	LIST_ENTRY(pf_task_proc) list;

	struct pf_task_conf *conf;

	pid_t pid;

	int ctl_fd;
	neb_evdp_source_t evdp_s;
	struct pf_control_cmd_msg ctl_msg;
	struct pf_control_rsp_msg ctl_rsp;
};

extern struct pf_task_proc *pf_task_proc_new(struct pf_task_conf *ptc)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void pf_task_proc_evdp_detach(struct pf_task_proc *ptp)
	_nattr_nonnull((1));
extern void pf_task_proc_del(struct pf_task_proc *ptp)
	_nattr_nonnull((1));


extern struct pf_task_proc *pf_task_runtime_proc;

extern int pf_task_proc_run(void)
	_nattr_warn_unused_result;

#endif
