
#ifndef PFLOW_TASK_SIGNAL_H
#define PFLOW_TASK_SIGNAL_H 1

#include <nebase/cdefs.h>

extern int pf_task_setup_signal_handlers(void)
	_nattr_warn_unused_result;
extern int pf_task_unblock_signals(void)
	_nattr_warn_unused_result;

#endif
