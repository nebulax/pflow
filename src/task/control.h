
#ifndef PFLOW_TASK_CONTROL_H
#define PFLOW_TASK_CONTROL_H 1

#include <nebase/cdefs.h>
#include <nebase/evdp/types.h>

enum {
	PF_CTL_CMD_NOOP = 0,
	PF_CTL_CMD_PING,
	PF_CTL_CMD_QUIT,
	PF_CTL_CMD_START,
	PF_CTL_CMD_STOP,
	PF_CTL_CMD_LOAD,
};

struct pf_control_cmd_msg {
	int type;
	int id;
};

enum {
	PF_CTL_RSP_NONE = 0,
	PF_CTL_RSP_PONG,
	PF_CTL_RSP_ERR,
	PF_CTL_RSP_OK,
};

struct pf_control_rsp_msg {
	int type;
	int id;
};

extern neb_evdp_cb_ret_t pf_task_ctl_hup(int fd, void *udata, const void *context);
extern neb_evdp_cb_ret_t pf_task_ctl_handle(int fd, void *udata, const void *context);

extern void pf_task_ctl_clean(void);

#endif
