
#include <nebase/syslog.h>
#include <nebase/sem.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>

#include "task.h"
#include "task/main.h"
#include "control.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>

struct pf_task_proc_l pf_task_proc_list = LIST_HEAD_INITIALIZER();

int pf_task_proc_sem = -1;

static int proc_list_allocate_all(void)
{
	int count = 0;
	struct pf_task_conf *ptc, *t;
	LIST_FOREACH_SAFE(ptc, &pf_task_conf_list, list, t) {
		struct pf_task_proc *proc = pf_task_proc_new(ptc);
		if (!proc) {
			neb_syslog(LOG_ERR, "Failed to get new task_proc struct");
			return -1;
		}
		LIST_REMOVE(ptc, list);
		LIST_INSERT_HEAD(&pf_task_proc_list, proc, list);
		count++;
	}

	return count;
}

static void proc_list_release_all(void)
{
	struct pf_task_proc *ptp, *t;
	LIST_FOREACH_SAFE(ptp, &pf_task_proc_list, list, t) {
		pf_task_proc_del(ptp);
		LIST_REMOVE(ptp, list);
	}
}

static int do_create_proc(struct pf_task_proc *ptp)
{
	int fds[2];
	if (socketpair(AF_UNIX, SOCK_SEQPACKET | SOCK_NONBLOCK | SOCK_CLOEXEC, 0, fds) == -1) {
		neb_syslog(LOG_ERR, "socketpair: %m");
		return -1;
	}

	ptp->pid = fork();
	if (ptp->pid == -1) {
		neb_syslog(LOG_ERR, "fork: %m");
		return -1;
	} else if (ptp->pid == 0) {
		close(fds[0]);
		ptp->ctl_fd = fds[1];
		LIST_REMOVE(ptp, list);
		proc_list_release_all();

		pf_task_runtime_proc = ptp;
		int ret = pf_task_proc_run();
		pf_task_proc_del(pf_task_runtime_proc);
		exit(ret);
	} else {
		close(fds[1]);
		ptp->ctl_fd = fds[0];

		struct timespec ts = {.tv_sec = 2};
		if (neb_sem_proc_wait_count(pf_task_proc_sem, 0, 1, &ts) != 0) {
			neb_syslog(LOG_ERR, "Failed to wait child process %d to be ready", ptp->ctl_fd);
			return -1;
		}

		return 0;
	}
}

int pf_task_proc_create(void)
{
	atexit(proc_list_release_all);
	int proc_count = proc_list_allocate_all();
	if (proc_count < 0) {
		neb_syslog(LOG_ERR, "Failed to get proc list");
		return -1;
	}
	if (proc_count == 0) {
		neb_syslog(LOG_ERR, "No proc to create");
		return -1;
	}

	pf_task_proc_sem = neb_sem_proc_create(NULL, 1);
	if (pf_task_proc_sem < 0) {
		neb_syslog(LOG_ERR, "Failed to create sem");
		return -1;
	}

	int ret = 0;
	struct pf_task_proc *ptp, *t;
	LIST_FOREACH_SAFE(ptp, &pf_task_proc_list, list, t) {
		if (do_create_proc(ptp) != 0) {
			neb_syslog(LOG_ERR, "Failed to create process"); // TODO description
			ret = -1;
			break;
		}
	}

	neb_sem_proc_destroy(pf_task_proc_sem);
	pf_task_proc_sem = -1;

	return ret;
}

static void parse_wstatus(pid_t pid, int wstatus)
{
	if (WIFSIGNALED(wstatus)) {
		if (WCOREDUMP(wstatus)) {
			neb_syslog(LOG_ERR, "Child %d is killed by signal %d and coredumped", pid, WTERMSIG(wstatus));
		} else {
			neb_syslog(LOG_ERR, "Child %d is killed by signal %d", pid, WTERMSIG(wstatus));
		}
	}
	if (WIFEXITED(wstatus)) {
		int status = WEXITSTATUS(wstatus);
		if (status != 0) {
			neb_syslog(LOG_ERR, "Child %d exit with error status %d", pid, status);
		} else {
			neb_syslog(LOG_DEBUG, "Child %d exit OK", pid);
		}
	}
}

static bool child_has_exited(pid_t pid)
{
	int wstatus = 0;
	pid_t epid = waitpid(pid, &wstatus, WNOHANG);
	switch (epid) {
	case -1:
		neb_syslogl(LOG_ERR, "waitpid(%d): %m", pid);
		return true;
	case 0:
		return false;
		break;
	default:
		parse_wstatus(pid, wstatus);
		return true;
		break;
	}
}

void pf_task_proc_destroy(void)
{
	struct pf_task_proc *ptp, *t;
	LIST_FOREACH_SAFE(ptp, &pf_task_proc_list, list, t) {
		if (ptp->pid <= 0)
			continue;
		if (child_has_exited(ptp->pid)) {
			ptp->pid = -1;
			continue;
		}
		if (kill(ptp->pid, SIGTERM) == -1)
			neb_syslogl(LOG_ERR, "kill(%d): %m", ptp->pid);
	}

	for (int i = 500; i > 0; i--) { // wait 5s
		int wstatus = 0;
		pid_t cpid = waitpid(-1, &wstatus, WNOHANG);
		switch (cpid) {
		case -1:
			if (errno != ECHILD)
				neb_syslogl(LOG_ERR, "waitpid(-1): %m");
			i = 1;
			continue;
			break;
		case 0:
			break;
		default:
			parse_wstatus(cpid, wstatus);
			break;
		}
		usleep(10000); // 10ms
	}
}

static int remove_proc_source(neb_evdp_source_t s)
{
	struct pf_task_proc *ptp = neb_evdp_source_get_udata(s);
	if (!ptp)
		return 0;
	pf_task_proc_evdp_detach(ptp);
	return 0;
}

static neb_evdp_cb_ret_t on_ctl_established(int fd _nattr_unused, void *udata, const void *context _nattr_unused)
{
	struct pf_task_proc *ptp = udata;
	return pf_ctl_start_task(ptp);
}

int pf_task_proc_register(neb_evdp_queue_t eq)
{
	struct pf_task_proc *ptp, *t;
	LIST_FOREACH_SAFE(ptp, &pf_task_proc_list, list, t) {
		ptp->evdp_s = neb_evdp_source_new_os_fd(ptp->ctl_fd, neb_evdp_sock_log_on_hup);
		if (!ptp->evdp_s) {
			neb_syslog(LOG_ERR, "Failed to create new evdp_source");
			return -1;
		}
		neb_evdp_source_set_udata(ptp->evdp_s, ptp);
		neb_evdp_source_set_on_remove(ptp->evdp_s, remove_proc_source);

		if (neb_evdp_queue_attach(eq, ptp->evdp_s) != 0) {
			neb_syslog(LOG_ERR, "Failed to attach evdp_source to evdp_queue");
			pf_task_proc_evdp_detach(ptp);
			return -1;
		}

		if (neb_evdp_source_os_fd_next_write(ptp->evdp_s, on_ctl_established) != 0) {
			neb_syslog(LOG_ERR, "Failed to set write action");
			pf_task_proc_evdp_detach(ptp);
			return -1;
		}
	}

	return 0;
}
