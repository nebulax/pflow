
#ifndef PFLOW_CONF_COMMON_H
#define PFLOW_CONF_COMMON_H 1

#include <nebase/cdefs.h>

#include <stdio.h>

#include <yaml.h>

extern const char *pf_default_config_dir;
extern const char *pf_default_output_dir;
extern const char *pf_default_plugin_dir;

extern int pf_conf_set_default_dir(const char *conf_file)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void pf_conf_clr_default_dir(void);

extern int pf_yml_skip_invalid_key(const char *filename, yaml_parser_t *yp, const yaml_char_t *key, const yaml_event_t *yep)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3, 4));
/**
 * \param[in] key NULL if called in sequence
 */
typedef int (*pf_yml_on_each)(yaml_parser_t *yp, const char *filename, const yaml_char_t *key, const yaml_event_t *yep, void *udata);

extern int pf_yml_parse_stream(const char *filename, FILE *stream, pf_yml_on_each on_each, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));
extern int pf_yml_parse_mapping(const char *filename, yaml_parser_t *yp, pf_yml_on_each on_each, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));
extern int pf_yml_parse_sequence(const char *filename, yaml_parser_t *yp, pf_yml_on_each on_each, void *udata)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3));

/*
 * Common Validators
 */
/**
 * \return 0 if validate ok
 */
extern int pf_conf_validate_pcap_filter(const char *filter)
	_nattr_warn_unused_result _nattr_nonnull((1));

#endif
