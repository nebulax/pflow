
#ifndef PFLOW_CONF_MAIN_H
#define PFLOW_CONF_MAIN_H 1

#include <nebase/cdefs.h>

struct pf_main_conf_s {
	int no_ctl;
};

extern struct pf_main_conf_s pf_main_conf;

extern int pf_conf_load(const char *conf_file)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void pf_conf_release(void);

#endif
