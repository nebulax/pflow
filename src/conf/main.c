
#include <nebase/syslog.h>

#include "main.h"
#include "common.h"
#include "task.h"

#include <stdio.h>

struct pf_main_conf_s pf_main_conf = {
	.no_ctl = 0,
};

static int parse_main_conf_sections(yaml_parser_t *yp, const char *filename, const yaml_char_t *key, const yaml_event_t *yep, void *udata _nattr_unused)
{
	switch (key[0]) {
	case 't':
		if (strncmp((const char *)key, "tasks", 5) == 0) {
			if (yep->type != YAML_SEQUENCE_START_EVENT) {
				neb_syslog(LOG_ERR, "Value for tasks should be sequence, but not %d", yep->type);
				return -1;
			}
			if (pf_yml_parse_sequence(filename, yp, pf_conf_parse_tasks, NULL) != 0) {
				neb_syslog(LOG_ERR, "Failed to parse tasks");
				return -1;
			}
			return 0;
		}
		break;
	default:
		break;
	}

	return pf_yml_skip_invalid_key(filename, yp, key, yep);
}

int pf_conf_load(const char *conf_file)
{
	if (pf_conf_set_default_dir(conf_file) != 0) {
		neb_syslog(LOG_ERR, "Failed to set default directory");
		return -1;
	}

	FILE *stream = fopen(conf_file, "r");
	if (!stream) {
		neb_syslog(LOG_ERR, "fopen(%s): %m", conf_file);
		return -1;
	}
	if (pf_yml_parse_stream(conf_file, stream, parse_main_conf_sections, NULL) != 0) {
		neb_syslog(LOG_ERR, "Failed to parse conf file %s", conf_file);
		fclose(stream);
		return -1;
	}
	fclose(stream);

	return 0;
}

void pf_conf_release(void)
{
	pf_conf_release_tasks();

	pf_conf_clr_default_dir();
}
