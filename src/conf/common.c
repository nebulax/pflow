
#include "options.h"

#include <nebase/syslog.h>

#include "common.h"

#include <unistd.h>
#include <libgen.h>
#include <string.h>

#include <yaml.h>
#include <pcap/pcap.h>

const char *pf_default_config_dir = NULL;
const char *pf_default_output_dir = NULL;
const char *pf_default_plugin_dir = NULL;

int pf_conf_set_default_dir(const char *conf_file)
{
	char *tmp = strdup(conf_file);
	if (!tmp) {
		neb_syslog(LOG_ERR, "strdup: %m");
		return -1;
	}
	const char *conf_dir = dirname(tmp);
	pf_default_config_dir = strdup(conf_dir);
	if (!pf_default_config_dir) {
		neb_syslog(LOG_ERR, "strdup: %m");
		free(tmp);
		return -1;
	}
	free(tmp);

	pf_default_output_dir = get_current_dir_name();
	if (!pf_default_output_dir) {
		neb_syslog(LOG_ERR, "get_current_dir_name: %m");
		return -1;
	}

	pf_default_plugin_dir = strdup(PFLOW_PLUGIN_INSTALL_DIR);
	if (!pf_default_plugin_dir) {
		neb_syslog(LOG_ERR, "strdup: %m");
		return -1;
	}

	return 0;
}

void pf_conf_clr_default_dir(void)
{
	if (pf_default_plugin_dir) {
		free((void *)pf_default_plugin_dir);
		pf_default_plugin_dir = NULL;
	}
	if (pf_default_output_dir) {
		free((void *)pf_default_output_dir);
		pf_default_output_dir = NULL;
	}
	if (pf_default_config_dir) {
		free((void *)pf_default_config_dir);
		pf_default_config_dir = NULL;
	}
}

static void print_yaml_error(const char *conf_file, yaml_parser_t *parser)
{
	const char *error_type = "unknown";
	switch (parser->error) {
	case YAML_MEMORY_ERROR:
		neb_syslog(LOG_CRIT, "YAML Parser: Memory Error");
		return;
		break;
	case YAML_READER_ERROR:
		error_type = "Reader Error";
		break;
	case YAML_SCANNER_ERROR:
		error_type = "Scanner Error";
		break;
	case YAML_PARSER_ERROR:
		error_type = "Parser Error";
		break;
	default:
		neb_syslog(LOG_ERR, "YAML Parser: unknown error type %d", parser->error);
		return;
		break;
	}

	neb_syslog(LOG_ERR, "YAML %s: %s in %s:%lu:%lu %s in %s:%lu:%lu",
	           error_type, parser->problem, conf_file,
	           parser->problem_mark.line + 1, parser->problem_mark.column + 1,
	           parser->context, conf_file,
	           parser->context_mark.line + 1, parser->context_mark.column + 1);
}

static void pf_yml_log_position(const char *conf_file, yaml_parser_t *parser, const char *prefix, int pri)
{
	neb_syslog(pri, "%s %s:%lu:%lu", prefix, conf_file, parser->mark.line, parser->mark.column);
}

static int yml_skip_until(const char *filename, yaml_parser_t *yp, yaml_event_type_t type, int indent)
{
	int done = 0;
	do {
		yaml_event_t event;
		if (!yaml_parser_parse(yp, &event)) {
			print_yaml_error(filename, yp);
			return -1;
		}

		yaml_event_type_t event_type = event.type;
		yaml_event_delete(&event);

		if (yp->indent == indent && event_type == type)
			return 0;

		switch (event_type) {
		case YAML_STREAM_END_EVENT:
		case YAML_DOCUMENT_END_EVENT:
			done = 1;
			break;
		default:
			break;
		}
	} while (!done);

	neb_syslog(LOG_ERR, "No maching yml event %d found at indent %d", type, indent);
	return -1;
}

int pf_yml_skip_invalid_key(const char *filename, yaml_parser_t *yp, const yaml_char_t *key, const yaml_event_t *yep)
{
	neb_syslog(LOG_WARNING, "Invalid key %s found at %s:%lu:%lu", key, filename, yp->mark.line, yp->mark.column);
	switch (yep->type) {
	case YAML_SCALAR_EVENT:
		return 0;
	case YAML_MAPPING_START_EVENT:
		return yml_skip_until(filename, yp, YAML_MAPPING_END_EVENT, yp->indent);
	case YAML_SEQUENCE_START_EVENT:
		return yml_skip_until(filename, yp, YAML_SEQUENCE_END_EVENT, yp->indent);
	default:
		neb_syslog(LOG_CRIT, "Invalid value type %d for key %s", yep->type, key);
		return -1;
	}
}

int pf_yml_parse_stream(const char *filename, FILE *stream, pf_yml_on_each on_each, void *udata)
{
	yaml_parser_t parser;

	int done = 0;

	if (!yaml_parser_initialize(&parser)) {
		neb_syslog(LOG_ERR, "Failed to init yaml parser");
		return -1;
	}
	yaml_parser_set_input_file(&parser, stream);

	int ret = 0;
	while (!done) {
		yaml_event_t event;
		if (!yaml_parser_parse(&parser, &event)) {
			print_yaml_error(filename, &parser);
			ret = -1;
			goto exit_delete_parser;
		}

		switch (event.type) {
		case YAML_STREAM_START_EVENT:
			// may get encoding, but we don't care
			break;
		case YAML_STREAM_END_EVENT:
			done = 1;
			break;
		case YAML_DOCUMENT_START_EVENT:
			// may have tag directives, but we don't care
			break;
		case YAML_DOCUMENT_END_EVENT:
			break;
		case YAML_MAPPING_START_EVENT:
			ret = pf_yml_parse_mapping(filename, &parser, on_each, udata);
			if (ret != 0)
				done = 1;
			break;
		case YAML_MAPPING_END_EVENT:
			break;
		case YAML_SEQUENCE_START_EVENT:
			ret = pf_yml_parse_sequence(filename, &parser, on_each, udata);
			if (ret != 0)
				done = 1;
			break;
		case YAML_SEQUENCE_END_EVENT:
			break;
		default:
			break;
		}

		yaml_event_delete(&event);
	}

exit_delete_parser:
	yaml_parser_delete(&parser);
	return ret;
}

int pf_yml_parse_mapping(const char *filename, yaml_parser_t *yp, pf_yml_on_each on_each, void *udata)
{
	int ret = 0;

	yaml_char_t *key = NULL;
	for (;;) {
		yaml_event_t event;
		if (!yaml_parser_parse(yp, &event)) {
			print_yaml_error(filename, yp);
			ret = -1;
			break;
		}

		if (event.type == YAML_MAPPING_END_EVENT) {
			yaml_event_delete(&event);
			break;
		}

		int is_key = yp->simple_key_allowed;
		if (!is_key) {
			if (event.type != YAML_SCALAR_EVENT) {
				neb_syslog(LOG_ERR, "Only scalar key type is supported");
				yaml_event_delete(&event);
				ret = -1;
				break;
			}
			if (key)
				free(key);
			key = (yaml_char_t *)strdup((const char *)event.data.scalar.value);
			if (!key) {
				neb_syslog(LOG_ERR, "strdup: %m");
				yaml_event_delete(&event);
				ret = -1;
				break;
			}
		} else {
			if (on_each(yp, filename, key, &event, udata) != 0) {
				pf_yml_log_position(filename, yp, "Error occure while parsing", LOG_ERR);
				yaml_event_delete(&event);
				ret = -1;
				break;
			}
		}

		yaml_event_delete(&event);
	}

	if (key)
		free(key);
	return ret;
}

int pf_yml_parse_sequence(const char *filename, yaml_parser_t *yp, pf_yml_on_each on_each, void *udata)
{
	int ret = 0;

	for (;;) {
		yaml_event_t event;
		if (!yaml_parser_parse(yp, &event)) {
			print_yaml_error(filename, yp);
			ret = -1;
			break;
		}

		if (event.type == YAML_SEQUENCE_END_EVENT) {
			yaml_event_delete(&event);
			break;
		}

		if (on_each(yp, filename, NULL, &event, udata) != 0) {
			pf_yml_log_position(filename, yp, "Error occure while parsing", LOG_ERR);
			yaml_event_delete(&event);
			ret = -1;
			break;
		}

		yaml_event_delete(&event);
	}

	return ret;
}

int pf_conf_validate_pcap_filter(const char *filter)
{
	struct bpf_program bp;
	/*
	 * pcap_compile_nopcap — Prototype:
	 * int pcap_compile_nopcap(int snaplen, int linktype, struct bpf_program *fp,char *str, int optimize, bpf_u_int32 netmask)
	 */
	if (pcap_compile_nopcap(-1, DLT_EN10MB, &bp, filter, 1, PCAP_NETMASK_UNKNOWN) == -1) {
		neb_syslog(LOG_ERR, "pcap_compile_nopcap failed");
		return -1;
	}
	pcap_freecode(&bp);
	return 0;
}
