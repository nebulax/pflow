
#include "options.h"

#include <nebase/syslog.h>

#include "task.h"
#include "common.h"
#include "task/libpcap.h"

#include <string.h>
#include <stdio.h>

struct pf_task_conf_l pf_task_conf_list = LIST_HEAD_INITIALIZER();
int pf_task_conf_count = 0;

const char *pf_conf_task_driver_name(int driver)
{
	switch (driver) {
	case PF_PCAP_DRIVER_IDLE:
		return "pcap_idle";
	case PF_PCAP_DRIVER_LIBPCAP:
		return "pcap_libpcap";
	case PF_PCAP_DRIVER_PFPACKET:
		return "pcap_pfpacket";
	case PF_PCAP_DRIVER_NONE:
	default:
		return "pcap_none";
	}
}

struct pf_task_conf *pf_conf_task_new(void)
{
	struct pf_task_conf *ptc = calloc(1, sizeof(struct pf_task_conf));
	if (!ptc) {
		neb_syslog(LOG_ERR, "calloc: %m");
		return NULL;
	}

	ptc->index = pf_task_conf_count++;
	ptc->conf_file = NULL;
	ptc->context = NULL;

	return ptc;
}

void pf_conf_task_del(struct pf_task_conf *ptc)
{
	if (ptc->context) {
		switch (ptc->driver) {
		case PF_PCAP_DRIVER_LIBPCAP:
			pf_task_libpcap_del_conf(ptc->context);
			break;
		default:
			neb_syslog(LOG_CRIT, "Task context is set but we don't know how to free it");
			break;
		}
		ptc->context = NULL;
	}
	if (ptc->conf_file) {
		free(ptc->conf_file);
		ptc->conf_file = NULL;
	}

	free(ptc);
}

static int parse_task_driver(struct pf_task_conf *ptc, const yaml_event_t *yep, const char *filename _nattr_unused)
{
	if (yep->type != YAML_SCALAR_EVENT) {
		neb_syslog(LOG_ERR, "Invalid value type %d for task driver", yep->type);
		return -1;
	}

	if (ptc->driver != PF_PCAP_DRIVER_NONE) {
		neb_syslog(LOG_ERR, "Duplicate task driver");
		return -1;
	}

	const char *driver = (const char *)yep->data.scalar.value;

	switch (driver[0]) {
	case 'i':
		if (strcmp(driver, "idle") == 0) {
			ptc->driver = PF_PCAP_DRIVER_IDLE;
			ptc->context = NULL;
			break;
		}
		break;
	case 'l':
		if (strcmp(driver, "libpcap") == 0) {
			ptc->driver = PF_PCAP_DRIVER_LIBPCAP;
			ptc->context = pf_task_libpcap_new_conf();
			if (!ptc->context) {
				neb_syslog(LOG_ERR, "Failed to allocate new libpcap task conf");
				return -1;
			}
			break;
		}
		break;
	default:
		neb_syslog(LOG_ERR, "Invalid task driver %s", driver);
		break;
	}

	return 0;
}

static int parse_task_uuid(struct pf_task_conf *ptc, const yaml_event_t *yep, const char *filename _nattr_unused)
{
	if (yep->type != YAML_SCALAR_EVENT) {
		neb_syslog(LOG_ERR, "Invalid value type %d for task uuid", yep->type);
		return -1;
	}

	const char *uuid = (const char *)yep->data.scalar.value;

	if (uuid_parse(uuid, ptc->uuid) != 0) {
		neb_syslog(LOG_ERR, "Invalid task uuid value %s", uuid);
		return -1;
	}

	return 0;
}

static int parse_driver_conf(yaml_parser_t *yp, const char *filename, const yaml_char_t *key, const yaml_event_t *yep, struct pf_task_conf *ptc)
{
	int ret = 0;
	switch (ptc->driver) {
	case PF_PCAP_DRIVER_NONE:
		ret = pf_yml_skip_invalid_key(filename, yp, key, yep);
		neb_syslog(LOG_WARNING, "You may need to specify the task driver to fix this");
		break;
	case PF_PCAP_DRIVER_IDLE:
		ret = pf_yml_skip_invalid_key(filename, yp, key, yep);
		break;
	case PF_PCAP_DRIVER_LIBPCAP:
		ret = pf_task_libpcap_parse_conf(yp, filename, key, yep, ptc);
		break;
	default:
		neb_syslog(LOG_CRIT, "No conf handler available for task driver %d", ptc->driver);
		ret = -1;
		break;
	}

	return ret;
}

static int parse_task_conf(yaml_parser_t *yp, const char *filename, const yaml_char_t *key, const yaml_event_t *yep, void *udata)
{
	struct pf_task_conf *ptc = udata;
	switch (key[0]) {
	case 'd':
		if (strcmp((const char *)key, "driver") == 0)
			return parse_task_driver(ptc, yep, filename);
		break;
	case 'u':
		if (strcmp((const char *)key, "uuid") == 0)
			return parse_task_uuid(ptc, yep, filename);
		break;
	default:
		break;
	}

	return parse_driver_conf(yp, filename, key, yep, ptc);
}

static int finalize_task_conf(struct pf_task_conf *ptc)
{
	int ret = 0;

	if (uuid_is_null(ptc->uuid))
		uuid_generate(ptc->uuid);

	switch (ptc->driver) {
	case PF_PCAP_DRIVER_NONE:
		break;
	case PF_PCAP_DRIVER_LIBPCAP:
		ret = pf_task_libpcap_finalize_conf(ptc);
		break;
	default:
		break;
	}

	return ret;
}

static int open_and_parse_task_file(const char *filepath, struct pf_task_conf *ptc)
{
	char *filename = NULL;
	if (filepath[0] == '/') {
		filename = strdup(filepath);
		if (!filename) {
			neb_syslog(LOG_ERR, "strdup: %m");
			return -1;
		}
	} else {
		if (asprintf(&filename, "%s/%s", pf_default_config_dir, filepath) == -1) {
			neb_syslog(LOG_ERR, "asprintf: %m");
			return -1;
		}
	}

	FILE *stream = fopen(filename, "r");
	if (!stream) {
		neb_syslog(LOG_ERR, "fopen(%s): %m", filename);
		free(filename);
		return -1;
	}

	if (pf_yml_parse_stream(filename, stream, parse_task_conf, ptc) != 0) {
		fclose(stream);
		free(filename);
		return -1;
	}
	fclose(stream);

	if (finalize_task_conf(ptc) != 0) {
		neb_syslog(LOG_ERR, "Failed to finalize task in task file %s", filename);
		free(filename);
		return -1;
	}
	ptc->conf_file = filename;

	return 0;
}

int pf_conf_parse_tasks(yaml_parser_t *yp, const char *filename, const yaml_char_t *key _nattr_unused, const yaml_event_t *yep, void *udata _nattr_unused)
{
	struct pf_task_conf *ptc = pf_conf_task_new();
	if (!ptc) {
		neb_syslog(LOG_ERR, "Failed to allocate new task conf struct");
		return -1;
	}

	switch (yep->type) {
	case YAML_SCALAR_EVENT:
		if (open_and_parse_task_file((const char *)yep->data.scalar.value, ptc) != 0) {
			neb_syslog(LOG_ERR, "Failed to parse task conf file: %s", yep->data.scalar.value);
			pf_conf_task_del(ptc);
			return -1;
		}
		LIST_INSERT_HEAD(&pf_task_conf_list, ptc, list);
		break;
	case YAML_MAPPING_START_EVENT:
		if (pf_yml_parse_mapping(filename, yp, parse_task_conf, ptc) != 0) {
			pf_conf_task_del(ptc);
			return -1;
		}
		if (finalize_task_conf(ptc) != 0) {
			neb_syslog(LOG_ERR, "Failed to finalize task conf");
			pf_conf_task_del(ptc);
			return -1;
		}
		LIST_INSERT_HEAD(&pf_task_conf_list, ptc, list);
		break;
	default:
		neb_syslog(LOG_ERR, "Invalid value type %d for 'tasks'", yep->type);
		pf_conf_task_del(ptc);
		return -1;
		break;
	}

	return 0;
}

void pf_conf_release_tasks(void)
{
	struct pf_task_conf *ptc, *t;
	LIST_FOREACH_SAFE(ptc, &pf_task_conf_list, list, t) {
		LIST_REMOVE(ptc, list);
		pf_conf_task_del(ptc);
	}
}
