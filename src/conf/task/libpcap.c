
#include <nebase/syslog.h>

#include <src/conf/common.h>
#include <src/conf/task.h>

#include "libpcap.h"

#include <string.h>

struct pf_task_libpcap_conf *pf_task_libpcap_new_conf(void)
{
	struct pf_task_libpcap_conf *pc = calloc(1, sizeof(struct pf_task_libpcap_conf));
	if (!pc) {
		neb_syslog(LOG_ERR, "calloc: %m");
		return NULL;
	}

	return pc;
}

void pf_task_libpcap_del_conf(struct pf_task_libpcap_conf *pc)
{
	if (pc->filter) {
		free(pc->filter);
		pc->filter = NULL;
	}
	free(pc);
}

int pf_task_libpcap_parse_conf(yaml_parser_t *yp, const char *filename,
                               const yaml_char_t *key, const yaml_event_t *yep,
                               struct pf_task_conf *ptc)
{
	struct pf_task_libpcap_conf *pc = ptc->context;

	switch (key[0]) {
	case 'i':
		if (strcmp((const char *)key, "ifname") == 0) {
			if (yep->type != YAML_SCALAR_EVENT) {
				neb_syslog(LOG_ERR, "Value for ifname should be of scalar type");
				return -1;
			}
			const yaml_char_t *ifname = yep->data.scalar.value;
			int len = snprintf(pc->ifname, IF_NAMESIZE, "%s", ifname);
			if (len >= IF_NAMESIZE) {
				neb_syslog(LOG_ERR, "Invalid ifname %s: length overflow", ifname);
				return -1;
			}
			return 0;
		}
		break;
	case 'f':
		if (strcmp((const char *)key, "filter") == 0) {
			if (yep->type != YAML_SCALAR_EVENT) {
				neb_syslog(LOG_ERR, "Value for filter should be of scalar type");
				return -1;
			}
			const char *filter = (const char *)yep->data.scalar.value;
			if (pf_conf_validate_pcap_filter(filter) != 0) {
				neb_syslog(LOG_ERR, "Invalid pcap filter value");
				return -1;
			}
			pc->filter = strdup(filter);
			if (!pc->filter) {
				neb_syslog(LOG_ERR, "strdup: %m");
				return -1;
			}
			return 0;
		}
		break;
	default:
		break;
	}

	return pf_yml_skip_invalid_key(filename, yp, key, yep);
}

int pf_task_libpcap_finalize_conf(struct pf_task_conf *ptc)
{
	struct pf_task_libpcap_conf *pc = ptc->context;

	pc->ifindex = if_nametoindex(pc->ifname);
	if (pc->ifindex == 0) {
		neb_syslog(LOG_ERR, "if_nametoindex(%s): %m", pc->ifname);
		return -1;
	}

	return 0;
}
