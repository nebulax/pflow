
#ifndef PFLOW_CONF_TASK_LIBPCAP_H
#define PFLOW_CONF_TASK_LIBPCAP_H 1

#include <nebase/cdefs.h>

#include <src/conf/task.h>

#include <net/if.h>

#include <yaml.h>

struct pf_task_libpcap_conf {
	/* Config Entries */
	char ifname[IF_NAMESIZE];
	char *filter;

	/* Runtime Entries */
	unsigned int ifindex;
};

extern struct pf_task_libpcap_conf *pf_task_libpcap_new_conf(void)
	_nattr_warn_unused_result;
extern void pf_task_libpcap_del_conf(struct pf_task_libpcap_conf *pc)
	_nattr_nonnull((1));
extern int pf_task_libpcap_parse_conf(yaml_parser_t *yp, const char *filename,
                                      const yaml_char_t *key, const yaml_event_t *yep,
                                      struct pf_task_conf *ptc)
	_nattr_warn_unused_result _nattr_nonnull((1, 2, 3, 4, 5));
extern int pf_task_libpcap_finalize_conf(struct pf_task_conf *ptc)
	_nattr_warn_unused_result _nattr_nonnull((1));

#endif
