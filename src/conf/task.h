
#ifndef PFLOW_CONF_TASK_H
#define PFLOW_CONF_TASK_H 1

#include <nebase/cdefs.h>

enum {
	PF_PCAP_DRIVER_NONE = 0,
	PF_PCAP_DRIVER_IDLE,
	PF_PCAP_DRIVER_LIBPCAP,
	PF_PCAP_DRIVER_PFPACKET,
};
extern const char *pf_conf_task_driver_name(int driver)
	_nattr_const;

#include <stddef.h>
#include <sys/queue.h>

#include <uuid.h>

struct pf_task_conf {
	LIST_ENTRY(pf_task_conf) list;
	char *conf_file;

	uuid_t uuid;
	int index;
	int driver;

	void *context;
};

extern struct pf_task_conf *pf_conf_task_new(void)
	_nattr_warn_unused_result;
extern void pf_conf_task_del(struct pf_task_conf *ptc)
	_nattr_nonnull((1));

LIST_HEAD(pf_task_conf_l, pf_task_conf);

extern struct pf_task_conf_l pf_task_conf_list;
extern int pf_task_conf_count;

#include <yaml.h>

extern int pf_conf_parse_tasks(yaml_parser_t *yp, const char *filename, const yaml_char_t *key, const yaml_event_t *yep, void *udata);
extern void pf_conf_release_tasks(void);

#endif
