
#ifndef PFLOW_TASK_H
#define PFLOW_TASK_H 1

#include <nebase/cdefs.h>
#include <nebase/evdp/types.h>

#include "conf/task.h"
#include "task/main.h"

#include <sys/queue.h>

LIST_HEAD(pf_task_proc_l, pf_task_proc);

extern struct pf_task_proc_l pf_task_proc_list;
extern int pf_task_proc_sem;

extern int pf_task_proc_create(void)
	_nattr_warn_unused_result;
extern void pf_task_proc_destroy(void);

extern int pf_task_proc_register(neb_evdp_queue_t eq)
	_nattr_warn_unused_result _nattr_nonnull((1));

#endif
