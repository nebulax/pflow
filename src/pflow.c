
#include <nebase/syslog.h>
#include <nebase/signal.h>
#include <nebase/evdp/core.h>

#include "conf/main.h"
#include "task.h"
#include "control.h"

#include <stdio.h>
#include <getopt.h>

enum {
	OPTV_NO_CTL = 256,
};

const struct option pf_opts[] = {
	{"config", required_argument, NULL, 'c'},
	{"help", no_argument, NULL, 'h'},
	{"no-ctl", no_argument, NULL, OPTV_NO_CTL},
	{NULL, no_argument, NULL, 0},
};

static void usage_print(const char *cmd)
{
	fprintf(stdout, "Usage: %s [OPTION]\n", cmd);
	fprintf(stdout, "\n");
	fprintf(stdout, "Options:\n");
	fprintf(stdout, "  -c, --config=<conf_file>\n");
	fprintf(stdout, "  -h, --help\n");
	fprintf(stdout, "\n");
}

static int parse_arguments(int argc, char *argv[], int *ret)
{
	int opt;
	int opt_index = 0;
	int no_ctl = 0;

	const char *conf_file = NULL;

	*ret = 0;

	while ((opt = getopt_long(argc, argv, "c:h", pf_opts, &opt_index)) != -1) {
		switch (opt) {
		case 'c':
			conf_file = optarg;
			break;
		case 'h':
			usage_print(argv[0]);
			return -1;
			break;
		case OPTV_NO_CTL:
			no_ctl = 1;
			break;
		case '?':
		default:
			*ret = -1;
			usage_print(argv[0]);
			return -1;
			break;
		}
	}

	if (conf_file) {
		if (pf_conf_load(conf_file) != 0) {
			*ret = -1;
			return -1;
		}
	} else {
		*ret = -1;
		neb_syslog(LOG_ERR, "No conf file given");
		return -1;
	}

	// overwrite conffile
	if (no_ctl)
		pf_main_conf.no_ctl = 1;

	return 0;
}

static int unblock_signals(void)
{
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGINT);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGCHLD);

	if (sigprocmask(SIG_UNBLOCK, &set, NULL) == -1) {
		neb_syslog(LOG_CRIT, "sigprocmask: %m");
		return -1;
	}
	return 0;
}

static int setup_signal_handlers(void)
{
	struct sigaction act;

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaddset(&act.sa_mask, SIGTERM);
	act.sa_handler = neb_sigterm_handler;
	if (sigaction(SIGINT, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaddset(&act.sa_mask, SIGINT);
	act.sa_handler = neb_sigterm_handler;
	if (sigaction(SIGTERM, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_SIGINFO | SA_RESTART;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGCHLD);
	act.sa_sigaction = neb_sigchld_action;
	if (sigaction(SIGCLD, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}
	neb_sigchld_info._sifields._sichld.refresh = 1;

	return 0;
}

static neb_evdp_cb_ret_t handle_event(void *udata _nattr_unused)
{
	if (thread_events & T_E_CHLD) {
		if (neb_signal_proc_block_chld() != 0)
			return NEB_EVDP_CB_BREAK_ERR;
		pid_t cpid = neb_sigchld_info._sifields._sichld.pid;
		neb_syslog(LOG_ERR, "Child process %d exited unexpectedly", cpid);
		neb_sigchld_info._sifields._sichld.refresh = 1;
		thread_events ^= T_E_CHLD;
		if (neb_signal_proc_unblock_chld() != 0)
			return NEB_EVDP_CB_BREAK_ERR;
		thread_events |= T_E_QUIT; // just quit if child exit
	}
	if (thread_events & T_E_QUIT)
		return NEB_EVDP_CB_BREAK_EXP;
	return NEB_EVDP_CB_CONTINUE;
}

int main(int argc, char *argv[])
{
	int ret;
	atexit(pf_conf_release);
	if (parse_arguments(argc, argv, &ret) != 0)
		return -1;

	neb_signal_proc_block_all();
	if (setup_signal_handlers() != 0) {
		neb_syslog(LOG_ERR, "Failed to setup signal handlers");
		return -1;
	}

	if (pf_task_proc_create() != 0) {
		neb_syslog(LOG_ERR, "Failed to create task processes");
		ret = -1;
		goto exit_destroy_tasks;
	}

	neb_evdp_queue_t eq = neb_evdp_queue_create(0);
	if (!eq) {
		neb_syslog(LOG_ERR, "Failed to create evdp_queue");
		ret = -1;
		goto exit_destroy_tasks;
	}
	if (pf_task_proc_register(eq) != 0) {
		neb_syslog(LOG_ERR, "Failed to add task evdp_sources");
		ret = -1;
		goto exit_destroy_queue;
	}
	if (pf_ctl_init(eq) != 0) {
		neb_syslog(LOG_ERR, "Failed to init ctl evdp_source");
		ret = -1;
		goto exit_destroy_queue;
	}

	if (unblock_signals() != 0) {
		neb_syslog(LOG_ERR, "Failed to unblock signals");
		ret = -1;
		goto exit_deinit_ctl;
	}

	neb_evdp_queue_set_event_handler(eq, handle_event);
	if (neb_evdp_queue_run(eq) != 0) {
		neb_syslog(LOG_ERR, "Error occured while running evdp_queue");
		ret = -1;
	}

exit_deinit_ctl:
	pf_ctl_deinit();
exit_destroy_queue:
	neb_evdp_queue_destroy(eq);
exit_destroy_tasks:
	pf_task_proc_destroy();
	return ret;
}
