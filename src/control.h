
#ifndef PFLOW_CONTROL_H
#define PFLOW_CONTROL_H 1

#include <nebase/cdefs.h>
#include <nebase/evdp/types.h>

#include <src/task/main.h>

extern const char *pf_ctl_listen_path;

extern int pf_ctl_init(neb_evdp_queue_t eq)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void pf_ctl_deinit(void);

extern neb_evdp_cb_ret_t pf_ctl_start_task(struct pf_task_proc *ptp)
	_nattr_warn_unused_result _nattr_nonnull((1));

#endif
