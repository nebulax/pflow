
#include <nebase/syslog.h>
#include <nebase/sock/common.h>
#include <nebase/sock/unix.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>

#include <src/conf/main.h>
#include <src/conf/task.h>
#include <src/task/main.h>
#include <src/task/control.h>

#include "control.h"

#include <unistd.h>

struct task_ctl_context {
	neb_evdp_source_t user_s;
	int wait_finished;
};

const char *pf_ctl_listen_path = "/run/pflow.sock"; // TODO from cmake var

static neb_evdp_source_t pf_ctl_listen_s = NULL;
static int pf_ctl_listen_fd = -1;
static struct task_ctl_context *task_ctl_contexts = NULL;

static neb_evdp_cb_ret_t on_listen_hup(int fd, void *udata, const void *context)
{
	neb_evdp_sock_log_on_hup(fd, udata, context);
	return NEB_EVDP_CB_BREAK_ERR;
}

static neb_evdp_cb_ret_t on_listen_event(int fd, void *udata _nattr_unused, const void *context _nattr_unused)
{
	// TODO accept and handle event
	return NEB_EVDP_CB_CONTINUE;
}

int pf_ctl_init(neb_evdp_queue_t eq)
{
	task_ctl_contexts = calloc(pf_task_conf_count, sizeof(struct task_ctl_context));
	if (!task_ctl_contexts) {
		neb_syslog(LOG_ERR, "calloc: %m");
		return -1;
	}
	for (int i = 0; i < pf_task_conf_count; i++) {
		task_ctl_contexts[i].wait_finished = 1;
	}

	if (pf_main_conf.no_ctl)
		return 0;

	int fd = neb_sock_unix_new_binded(SOCK_SEQPACKET, pf_ctl_listen_path);
	if (fd < 0) {
		neb_syslog(LOG_ERR, "Failed to listen on %s", pf_ctl_listen_path);
		free(task_ctl_contexts);
		task_ctl_contexts = NULL;
		return -1;
	}
	pf_ctl_listen_fd = fd;

	pf_ctl_listen_s = neb_evdp_source_new_ro_fd(pf_ctl_listen_fd, on_listen_event, on_listen_hup);
	if (!pf_ctl_listen_s) {
		neb_syslog(LOG_ERR, "Failed to create listen evdp_s");
		close(pf_ctl_listen_fd);
		unlink(pf_ctl_listen_path);
		free(task_ctl_contexts);
		task_ctl_contexts = NULL;
		return -1;
	}

	if (neb_evdp_queue_attach(eq, pf_ctl_listen_s) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach listen evdp_s");
		pf_ctl_deinit();
		return -1;
	}

	return 0;
}

void pf_ctl_deinit(void)
{
	if (!pf_ctl_listen_s)
		return;
	close(pf_ctl_listen_fd);
	neb_evdp_queue_t q = neb_evdp_source_get_queue(pf_ctl_listen_s);
	if (q) {
		if (neb_evdp_queue_detach(q, pf_ctl_listen_s, 1) != 0)
			neb_syslog(LOG_ERR, "Failed to detach listen evdp_s");
	}
	neb_evdp_source_del(pf_ctl_listen_s);

	unlink(pf_ctl_listen_path);
	free(task_ctl_contexts);
	task_ctl_contexts = NULL;
}

static neb_evdp_cb_ret_t task_on_ctl_handled(int fd, void *udata, const void *context)
{
	struct pf_task_proc *ptp = udata;
	struct task_ctl_context *tcc = &task_ctl_contexts[ptp->conf->index];
	for (;;) {
		int nread = 0;
		if (neb_evdp_io_get_nread(context, &nread) != 0) {
			neb_syslog(LOG_ERR, "Failed to get unread rsp msg size from fd %d", fd);
			return NEB_EVDP_CB_BREAK_ERR;
		}
		if (nread != sizeof(ptp->ctl_rsp)) {
			neb_evdp_sock_log_on_hup(fd, udata, context);
			return NEB_EVDP_CB_CLOSE;
		}
		if (neb_sock_recv_exact(fd, &ptp->ctl_rsp, sizeof(ptp->ctl_rsp)) != 0) {
			neb_syslog(LOG_CRIT, "Failed to recv ctl response");
			return NEB_EVDP_CB_BREAK_ERR;
		}
		if (ptp->ctl_rsp.id == ptp->ctl_msg.id)
			break;
		neb_syslog(LOG_WARNING, "Task %d get response id %d while expecting %d",
		           ptp->conf->index, ptp->ctl_rsp.id, ptp->ctl_msg.id);
	}
	switch (ptp->ctl_msg.type) {
	case PF_CTL_CMD_START:
		// TODO
		break;
	default:
		// TODO
		break;
	}
	// TODO handle user_s
	ptp->ctl_msg.type = PF_CTL_CMD_NOOP;
	tcc->wait_finished = 1;
	return NEB_EVDP_CB_CONTINUE;
}

neb_evdp_cb_ret_t pf_ctl_start_task(struct pf_task_proc *ptp)
{
	struct task_ctl_context *tcc = &task_ctl_contexts[ptp->conf->index];
	if (!tcc->wait_finished) // do nothing, maybe already timeout
		return NEB_EVDP_CB_CONTINUE;
	tcc->wait_finished = 0;
	if (ptp->ctl_msg.type != PF_CTL_CMD_NOOP) {
		neb_syslog(LOG_ERR, "task %d is still handling cmd %d", ptp->conf->index, ptp->ctl_msg.type);
		return NEB_EVDP_CB_CONTINUE;
	}

	ptp->ctl_msg.type = PF_CTL_CMD_START;
	ptp->ctl_msg.id++;
	if (neb_sock_send_exact(ptp->ctl_fd, &ptp->ctl_msg, sizeof(ptp->ctl_msg)) != 0) {
		neb_syslog(LOG_CRIT, "Failed to send ctl msg");
		return NEB_EVDP_CB_BREAK_ERR;
	}
	ptp->ctl_rsp.type = PF_CTL_RSP_NONE;
	if (neb_evdp_source_os_fd_next_read(ptp->evdp_s, task_on_ctl_handled) != 0) {
		neb_syslog(LOG_CRIT, "Failed to set read handler");
		return NEB_EVDP_CB_BREAK_ERR;
	}
	return NEB_EVDP_CB_CONTINUE;
}
